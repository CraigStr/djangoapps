from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.http import HttpResponseRedirect
from .forms import UserRegistrationForm, UserLoginForm

import re


def register_view(request):
    if request.method == 'POST':
        form = UserRegistrationForm(request.POST)
        if form.is_valid():
            form.clean()
            user_obj = form.cleaned_data
            username = user_obj['username']
            first_name = user_obj['first_name']
            last_name = user_obj['last_name']
            email = user_obj['email']
            password1 = user_obj['password1']
            password2 = user_obj['password2']
            if not (User.objects.filter(username=username).exists() or User.objects.filter(email=email).exists()):
                user = User.objects.create_user(username, email, password1)
                user.first_name = first_name
                user.last_name = last_name
                user.save()
                user_login = authenticate(username=username, password=password1)
                login(request, user_login)
                messages.success(request, f'Account created for {username}!')
                return HttpResponseRedirect('/')
            else:
                messages.success(request, f'Username has already been taken!')
                return render(request, 'users/register.html', {'form': form})
    else:
        form = UserRegistrationForm()

    return render(request, 'users/register.html', {'form': form})


def login_view(request):
    if request.method == 'POST':
        form = UserLoginForm(request.POST)
        if form.is_valid():
            user_obj = form.cleaned_data
            username = user_obj['username']
            password = user_obj['password']
            user = authenticate(username=username, password=password)
            if user is None:
                messages.success(request, f'Username or Password is Incorrect')
                return render(request, 'users/login.html', {'form': form})
            else:
                login(request, user)
                messages.success(request, f'Welcome Back {username}')
            return HttpResponseRedirect('/')
        else:
            messages.success(request, f'Username or Password is Incorrect')
            return render(request, 'users/login.html', {'form': form})

    else:
        form = UserLoginForm()

    return render(request, 'users/login.html', {'form': form})


def logout_view(request):
    logout(request)
    messages.success(request, f'Logged Out')
    return redirect('homepage')
