from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.password_validation import CommonPasswordValidator, UserAttributeSimilarityValidator, \
    NumericPasswordValidator, MinimumLengthValidator

import re


class UserRegistrationForm(forms.Form):
    username = forms.CharField(
        required=True,
        label='Username',
        max_length=50
    )
    email = forms.CharField(
        required=True,
        label='Email',
        max_length=100,
    )
    first_name = forms.CharField(
        required=True,
        label='First Name',
        max_length=100,
    )
    last_name = forms.CharField(
        required=True,
        label='First Name',
        max_length=100,
    )
    password1 = forms.CharField(
        required=True,
        label='Password',
        max_length=64,
        widget=forms.PasswordInput(),
    )
    password2 = forms.CharField(
        required=True,
        label='Confirm Password',
        max_length=64,
        widget=forms.PasswordInput(),
    )

    class Meta:
        model = User
        fields = (
            'username'
            'email'
            'first_name'
            'last_name'
            'password'
        )

    def save(self):
        data = self.cleaned_data
        user = User(username=data['username'],
                    email=data['email'],
                    first_name=data['first_name'],
                    last_name=data['last_name'],
                    password=data['password1'])
        user.save()

    def clean(self):
        cleaned_data = super().clean()
        username = cleaned_data.get('username')
        first_name = cleaned_data.get('first_name')
        last_name = cleaned_data.get('last_name')
        email = cleaned_data.get('email')
        password1 = cleaned_data.get('password1')
        password2 = cleaned_data.get('password2')
        if not (first_name and last_name):
            raise forms.ValidationError("Please enter a valid name")
        if password1 and password2:
            if password1 == password2:
                if UserAttributeSimilarityValidator().validate(password1):
                    raise forms.ValidationError("Attributes are too similar")
                if CommonPasswordValidator().validate(password1):
                    raise forms.ValidationError("Password is too common")
                if MinimumLengthValidator().validate(password1):
                    raise forms.ValidationError("Password must be at least 8 characters long")
                if NumericPasswordValidator().validate(password1):
                    raise forms.ValidationError("Password cant be all numbers")
            else:
                raise forms.ValidationError("Passwords do not match")

        if not re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", email):
            raise forms.ValidationError("Please enter a valid email address")
        if not username:
            raise forms.ValidationError("Please enter a valid username")


class UserLoginForm(forms.Form):
    username = forms.CharField(
        required=True,
        label='Username',
        max_length=50
    )
    password = forms.CharField(
        required=True,
        label='Password',
        max_length=64,
        widget=forms.PasswordInput()
    )
