# DjangoApps

### Current Apps
* [RiverLevels](/riverlevels/)  
  * [Under Maintenance](#)
* [Movies (WIP)](/movies/)
  * [Live Version](https://www.craigstratford.me/movies)
* [Users (WIP)](/users/)
  * [Live Version](https://www.craigstratford.me/users/register)

<sup>
These apps are currently in this repository in their raw state.\
Eventually I will convert them all into reusable app that can simply be installed with pip
</sup>

## RiverLevels
[Under Maintenance](#)  
A site to show the previous months river levels for locations across the Republic or Ireland. 
Data is taken from the Office of Public Works (OPW) API as a .csv file, parsed and fed into a Django backend to generate a MatPlotLib graph of the waterlevels.
The Leaflet javascript library is used to generate an interactive map to select from. 

## Movies (WIP)
[Live Version](https://www.craigstratford.me/movies)  
Displays current top movies and allows the user to view information about each movie, such as description, actors, release date, ratings, etc. Actors/Actresses can also be found from each movie and their other movies selected from the actors page.

Made to learn more about Django models and to correctly migrate Django from SQLite to PostgreSQL.

## Users (WIP)
[Live Version](https://www.craigstratford.me/users/register)  
Simple authentication app. Allows for:
* Register
* Login
* Logout

Future version will also have features to allow for:
* View profile
* Change password
* OAuth2
* 2 Factor Authentication
* Forgot your password