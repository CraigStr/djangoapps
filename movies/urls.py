from django.urls import path
from . import views

urlpatterns = [
    path("", views.movie_list_view, name='movielist'),
    path("filter/", views.filter_movies, name='moviefilter'),
    path("movie/<int:id>", views.movie_details,  name='moviedetails'),
    path("person/<int:id>", views.person_details,  name='persondetails'),
]
