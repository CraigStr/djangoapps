from django.shortcuts import render
from django.http import HttpResponse
from .models import MoviesFavourite, MoviesToWatch
import requests
import json

api_key = '################################'


# Create your views here.
def movie_list_view(request):
    popular_url = f"https://api.themoviedb.org/3/movie/popular?api_key={api_key}&language=en-US&page=1"
    if request.method == 'GET':
        js = requests.get(popular_url).json()
        js_dump = json.dumps(js)
        return render(request, "movies/movielist.html", {'data': js_dump, 'favourited': MoviesFavourite.objects.all(),
                                                         'to_watch': MoviesToWatch.objects.all()})
    elif request.method == 'POST':
        to_favourite = request.POST.get('toFavourite', False)
        remove_favourite = request.POST.get('removeFavourite', False)
        to_watch = request.POST.get('toWatch', False)
        remove_watch = request.POST.get('removeWatch', False)
        print(to_watch, remove_watch, to_favourite, remove_favourite, sep="\n")

        # print(to_favourite, to_watch, remove_favourite, remove_watch, sep="\n")

        if to_favourite:
            movie_json = requests.get(
                f'https://api.themoviedb.org/3/movie/{to_favourite}?api_key={api_key}&language=en-US').json()

            movie_title = movie_json['original_title']
            movie_overview = movie_json['overview']
            poster_path = movie_json['poster_path']
            vote_average = movie_json['vote_average']
            movie_genres = json.dumps(movie_json['genres'])
            movie_released = movie_json['release_date']
            print(movie_released)

            # print(movie_title, movie_overview, poster_path, vote_average, movie_genres, sep="\n")

            obj, created = MoviesFavourite.objects.get_or_create(movie_id=to_favourite, movie_title=movie_title,
                                                                 movie_overview=movie_overview, poster_path=poster_path,
                                                                 vote_average=vote_average,
                                                                 movie_genres=movie_genres,
                                                                 movie_released=movie_released)
            if created:
                print("C")
                return HttpResponse(status=201)
            else:
                print("N")
                return HttpResponse(status=409)

        elif to_watch:
            movie_json = requests.get(
                f'https://api.themoviedb.org/3/movie/{to_watch}?api_key={api_key}&language=en-US').json()

            movie_title = movie_json['original_title']
            movie_overview = movie_json['overview']
            poster_path = movie_json['poster_path']
            vote_average = movie_json['vote_average']
            movie_genres = json.dumps(movie_json['genres'])
            movie_released = movie_json['release_date']
            print(movie_released)

            obj, created = MoviesToWatch.objects.get_or_create(movie_id=to_watch, movie_title=movie_title,
                                                               movie_overview=movie_overview, poster_path=poster_path,
                                                               vote_average=vote_average,
                                                               movie_genres=movie_genres, movie_released=movie_released)
            if created:
                return HttpResponse(status=201)
            else:
                return HttpResponse(status=409)

        elif remove_favourite:
            movie_del = MoviesFavourite.objects.get(movie_id=remove_favourite)
            movie_del.delete()
            return HttpResponse(status=200)

        elif remove_watch:
            movie_del = MoviesToWatch.objects.get(movie_id=remove_watch)
            movie_del.delete()
            return HttpResponse(status=200)


def filter_movies(request):
    return render(request, 'movies/filterMovies.html', {})


def movie_details(request, id):
    url = f'https://api.themoviedb.org/3/movie/{id}?api_key={api_key}&language=en-GB&append_to_response=credits'
    response = requests.get(url).json()

    for x in response['credits']['crew']:
        if x['profile_path'] is None:
            x['profile_path'] = "EMPTY"
        if x['job'] == 'Director':
            response['director'] = x['name']

    cast = {}
    for x in response['credits']['cast'][:5:]:
        cast[x['name']] = x['character'], x['id']
    response['cast'] = cast

    # response.pop('credits')

    # pprint.pprint(response)
    return render(request, 'movies/moviedetails.html', {'data': response})


def person_details(request, id):
    person_bio = requests.get(f' https://api.themoviedb.org/3/person/{id}?api_key={api_key}&language=en-US').json()
    person_credits = requests.get(
        f'https://api.themoviedb.org/3/person/{id}/movie_credits?api_key={api_key}&language=en-US').json()

    person_bio['biography'] = person_bio['biography'].replace(u'\xa0', ' ')
    print(person_bio['biography'])

    return render(request, 'movies/persondetails.html', {'biography': person_bio, 'credits': person_credits})
