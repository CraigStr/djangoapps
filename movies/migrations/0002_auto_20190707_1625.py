# Generated by Django 2.1.5 on 2019-07-07 16:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='moviesfavourite',
            name='movie_id',
            field=models.PositiveIntegerField(default=0, unique=True),
        ),
        migrations.AlterField(
            model_name='moviestowatch',
            name='movie_id',
            field=models.PositiveIntegerField(default=0, unique=True),
        ),
    ]
