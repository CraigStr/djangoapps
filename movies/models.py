from django.db import models
from datetime import date


# Create your models here.
class MoviesFavourite(models.Model):
    movie_id = models.CharField(max_length=250, null=False, unique=True)
    movie_title = models.CharField(max_length=250, null=False, default="")
    movie_overview = models.TextField(null=False, default="")
    poster_path = models.CharField(max_length=250, null=False, default="")
    vote_average = models.DecimalField(max_digits=2, decimal_places=1, default=0)
    movie_genres = models.CharField(max_length=1000, null=False, default=0)
    movie_released = models.DateField(null=False, default=date.today)

    class Meta:
        verbose_name = 'Favourite Movie'
        verbose_name_plural = 'Favourites Movies'

    def __str__(self):
        return str(self.movie_id)


class MoviesToWatch(models.Model):
    movie_id = models.CharField(max_length=250, null=False, unique=True)
    movie_title = models.CharField(max_length=250, null=False, default="")
    movie_overview = models.TextField(null=False, default="")
    poster_path = models.CharField(max_length=250, null=False, default="")
    vote_average = models.DecimalField(max_digits=2, decimal_places=1, default=0)
    movie_genres = models.CharField(max_length=1000, null=False, default=0)
    movie_released = models.DateField(null=False, default=date.today)

    class Meta:
        verbose_name = 'Movie to Watch'
        verbose_name_plural = 'Movies to Watch'

    def __str__(self):
        return self.movie_id
