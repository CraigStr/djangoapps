from . import models
from django.contrib import admin


class MoviesFavouriteAdmin(admin.ModelAdmin):
    list_display = ["movie_id"]


class MoviesToWatchAdmin(admin.ModelAdmin):
    list_display = ['movie_id']


admin.site.register(models.MoviesFavourite, MoviesFavouriteAdmin)
admin.site.register(models.MoviesToWatch, MoviesToWatchAdmin)
