function populatePopularMovies(json) {
    let movie_list = [];
    for (const key in json['results']) {
        const image_path = 'https://image.tmdb.org/t/p/w300/' + json["results"][key]["poster_path"];
        movie_list.push(
            // ' + json["results"][key]["original_title"] + '
            '<div class="col s12 m3 l2">' +
            '   <div class="card">' +
            '       <div class="card-image waves-effect waves-block waves-light">' +
            '           <img class="activator responsive-img" src="' + image_path + '">' +
            '       </div>' +
            '       <div class="card-content">' +
            '           <span class="card-title activator grey-text text-darken-4"><p class="truncate">' + json["results"][key]["original_title"] + '</p></span>' +
            '           <a class="btn-floating waves-effect waves-light favouriteBtn" data-movie-fav-id="' + json["results"][key]["id"] + '"><i class="material-icons">favorite_border</i></a>' +
            '           <a class="btn-floating waves-effect waves-light watchBtn" data-movie-watch-id="' + json["results"][key]["id"] + '"><i class="material-icons">star_border</i></a>' +
            '           <a href="movie/' + json["results"][key]["id"] + '" class="btn-floating right waves-effect waves-light red"><i class="material-icons">more_horiz</i></a>' +
            '       </div>' +
            '       <div class="card-reveal">' +
            '           <span class="card-title grey-text text-darken-4">' + json["results"][key]["original_title"] + '<i class="material-icons right">close</i></span>' +
            '           <p>' + json["results"][key]["overview"] + '</p>' +
            '       </div>' +
            '    </div>' +
            '</div>'
        )
    }
    $('#popular').html(movie_list.join(""));
}

$(document).on('click', '.favouriteBtn', function () {
    const id = $(this).attr('data-movie-fav-id');

    if ($(this).children().first().text() === 'favorite_border') {
        console.log("CreateFav");
        $.ajax({
            url: '',
            type: 'post',
            data: {toFavourite: $(this).attr('data-movie-fav-id')},
            success: function (data, status, xhr) {
                $('[data-movie-fav-id=' + id + '] i').text('favorite');
            },
            error: function (data, status, xhr) {
                console.log(data.status)
            }
        })
    } else {
        console.log("DeleteFav");
        $.ajax({
            url: '',
            type: 'post',
            data: {removeFavourite: $(this).attr('data-movie-fav-id')},
            success: function (data, status, xhr) {
                $('[data-movie-fav-id=' + id + '] i').text('favorite_border');
                $('.favourite-body').first($('[data-movie-fav-id=' + id + ']')).closest($('.col')).remove()

            },
            error: function (data, status, xhr) {
                console.log(data.status)
            }
        })
    }
});

$(document).on('click', '.watchBtn', function () {
    const id = $(this).attr('data-movie-watch-id');
    if ($(this).children().first().text() === 'star_border') {
        console.log("CreateWatch");
        $.ajax({
            url: '',
            type: 'post',
            data: {toWatch: $(this).attr('data-movie-watch-id')},
            success: function (data, status, xhr) {
                $('[data-movie-watch-id=' + id + '] i').text('star');
            },
            error: function (data, status, xhr) {
                console.log(data.status)
            }
        })
    } else {
        console.log("DeleteWatch");
        $.ajax({
            url: '',
            type: 'post',
            data: {removeWatch: $(this).attr('data-movie-watch-id')},
            success: function (data, status, xhr) {
                $('[data-movie-watch-id=' + id + '] i').text('star_border');
            },
            error: function (data, status, xhr) {
                console.log(data.status)
            }
        })
    }
});

