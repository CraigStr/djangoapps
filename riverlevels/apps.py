from django.apps import AppConfig


class RiverlevelsConfig(AppConfig):
    name = 'riverlevels'
