from django.urls import path
from . import views

urlpatterns = [
    path("", views.riverlevel_view, name='river'),
]
