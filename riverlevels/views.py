from django.shortcuts import render
from django.http import HttpResponse
import json
import requests


def riverlevel_view(request):
    if request.method == 'GET' and 'loc' in request.GET:
        location = request.GET['loc']
        return HttpResponse(save_map(location))

    # r = requests.get('http://waterlevel.ie/geojson/latest/')
    js_dump = ""
    with open('static/riverlevels/SavedData/locations.json') as json_file:
        js_data = json.load(json_file)
        js_dump = json.dumps(js_data).replace('\'', '\\\'')
    return render(request, 'riverlevels/index.html', {'data': js_dump})


def save_map(name):
    import matplotlib.pyplot as plt
    import matplotlib.dates as mpldates
    import csv
    import datetime

    with open('static/riverlevels/SavedData/csv_urls.json', 'r') as urls_csv:
        urls = json.load(urls_csv)
        url = 'https://waterlevel.ie' + urls[name]
        with requests.Session() as s:
            download = s.get(url)
            decoded_content = download.content.decode('utf-8')

            reader = csv.reader(decoded_content.splitlines(), delimiter=',')
            lines = list(reader)[1:]

            date_obj = []
            values = []

            for row in lines:
                date_obj.append(datetime.datetime.strptime(row[0], "%Y-%m-%d %H:%M"))
                values.append(float(row[1]))

            dates = mpldates.date2num(date_obj)
            plt.figure(figsize=(50, 25))
            plt.plot_date(dates, values, 'b-')
            plt.grid()
            plt.gca().set_xticks(date_obj)

            plt.xlabel('date')
            plt.ylabel('level (meters)')
            formatter = mpldates.DateFormatter('%Y-%m-%d %H:%M:%S')
            plt.gcf().axes[0].xaxis.set_major_formatter(formatter)
            plt.gcf().autofmt_xdate()
            plt.title('Recent Water Levels for:\n' + name)
            plt.savefig('static/riverlevels/Plots/' + name.replace(" ", '-') + '.png', bbox_inches='tight', pad_inches=0)
            return 'riverlevels/Plots/' + name.replace(" ", "-") + '.png'
